<?php

namespace BDSA\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        $authUtils = $this->get('security.authentication_utils');

        return $this->render('BDSAUserBundle:Security:login.html.twig', array(
            'last_email' => $authUtils->getLastUsername(),
            'error'      => $authUtils->getLastAuthenticationError()
        ));
    }
    public function loginCheckAction(Request $request)
    {

    }

    public function logoutAction(Request $request)
    {

    }
}