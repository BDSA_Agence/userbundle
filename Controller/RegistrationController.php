<?php

namespace BDSA\UserBundle\Controller;

use BDSA\UserBundle\Form\UserType;
use BDSA\UserBundle\Event\UserEvent;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class RegistrationController extends Controller
{
    public function registerAction(Request $request)
    {
        $dispatcher = $this->get('event_dispatcher');

        $class = $this->container->getParameter('bdsa_user.class');
        $user = new $class;
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $user->setUsername(md5(uniqid()));

            $userEvent = new UserEvent($user);
            

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);

            $dispatcher->dispatch(UserEvent::USER_REGISTRATION_PREFLUSH, $userEvent);

            $em->flush();

            $dispatcher->dispatch(UserEvent::USER_REGISTRATION_SUCCESS, $userEvent);

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('bdsa_user_list');
        }

        return $this->render('BDSAUserBundle:Registration:register.html.twig', array(
        		'form' => $form->createView()
        	)
        );
    }
}