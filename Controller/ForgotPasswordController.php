<?php

namespace BDSA\UserBundle\Controller;

use BDSA\UserBundle\Form\ForgotPasswordType;
use BDSA\UserBundle\Form\ForgotPasswordResetType;
use BDSA\UserBundle\Entity\User;
use BDSA\UserBundle\Event\UserEvent;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ForgotPasswordController extends Controller
{
    public function forgotPasswordAction(Request $request)
    {
        $class = $this->container->getParameter('bdsa_user.class');

        $dispatcher = $this->get('event_dispatcher');

        $form = $this->createForm(ForgotPasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $datas = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository($class)->findOneBy( array('email' => $datas['email']) );

            $userEvent = new UserEvent($user);
            $dispatcher->dispatch(UserEvent::USER_FORGOTPASSWORD_REQUEST, $userEvent);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('bdsa_user_login');
        }

        return $this->render('BDSAUserBundle:ForgotPassword:forgotPassword.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function forgotPasswordResetAction(Request $request, $token)
    {
        $class = $this->container->getParameter('bdsa_user.class');
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository($class)->findOneBy( array('forgot_password' => $token) );

        if( empty($user) )
            throw new NotFoundHttpException(sprintf('The user with "reset password token" does not exist for value "%s"', $token));

        $dispatcher = $this->get('event_dispatcher');

        $form = $this->createForm(ForgotPasswordResetType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $datas = $form->getData();

            $password = $this->get('security.password_encoder')->encodePassword($user, $datas['plainPassword']);
            $user->setPassword($password);
            $user->setForgotPassword(null);

            $userEvent = new UserEvent($user);
            $dispatcher->dispatch(UserEvent::USER_FORGOTPASSWORD_RESET, $userEvent);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('bdsa_user_login');
        }

        return $this->render('BDSAUserBundle:ForgotPassword:forgotPasswordReset.html.twig', array(
            'form' => $form->createView()
        ));
    }
}