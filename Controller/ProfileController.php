<?php

namespace BDSA\UserBundle\Controller;

use BDSA\UserBundle\Event\UserEvent;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\EventDispatcher\EventDispatcher;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProfileController extends Controller
{
    public function listAction(Request $request)
    {
        $class = $this->container->getParameter('bdsa_user.class');

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository($class)->findAll();

        return $this->render('BDSAUserBundle:Profile:list.html.twig', array(
            'users' => $users,
        ));
    }

    public function editAction(Request $request, $id)
    {
        $dispatcher = $this->get('event_dispatcher');

        $class = $this->container->getParameter('bdsa_user.class');

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository($class)->findOneBy(array('id' => $id));

        $deleteForm = $this->createDeleteForm($user);

        $editForm = $this->createForm('BDSA\UserBundle\Form\UserProfileType', $user);
        $editForm->handleRequest($request);

        if( $editForm->isSubmitted() && $editForm->isValid() )
        {
            $em = $this->getDoctrine()->getManager();

            $userEvent = new UserEvent($user);

            $em->persist($user);
            $em->flush();

            $dispatcher->dispatch(UserEvent::USER_PROFILE_EDIT_POSTFLUSH, $userEvent);

            return $this->redirectToRoute('bdsa_user_show');
        }

        return $this->render('BDSAUserBundle:Profile:edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function showAction(Request $request)
    {
        $user = $this->getUser();

        return $this->render('BDSAUserBundle:Profile:show.html.twig', array(
            'user' => $user
        ));
    }

    /**
     * Deletes a user entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $class = $this->container->getParameter('bdsa_user.class');

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository($class)->findOneBy(array('id' => $id));

        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('bdsa_user_list');
    }

    /**
     */
    protected function createDeleteForm($user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bdsa_user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}