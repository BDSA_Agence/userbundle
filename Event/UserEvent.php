<?php

namespace BDSA\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use BDSA\UserBundle\Entity\User;

class UserEvent extends Event
{
    const USER_REGISTRATION_SUCCESS = 'bdsa.user.registration.success';
    const USER_REGISTRATION_PREFLUSH = 'bdsa.user.registration.preflush';

    const USER_FORGOTPASSWORD_REQUEST = 'bdsa.user.forgotpassword.request';
    const USER_FORGOTPASSWORD_RESET = 'bdsa.user.forgotpassword.reset';

    const USER_PROFILE_EDIT_POSTFLUSH = 'bdsa.user.profile.edit.postflush';

    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}