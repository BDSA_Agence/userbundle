# BDSA\UserBundle

## Composer.json

```json
"repositories": [
{
    "type": "vcs",
    "url": "https://BDSA_Agence@bitbucket.org/BDSA_Agence/userbundle.git"
}

#...

"require": {
"bdsa/user-bundle": "dev-master"
```

## Define BDSA\UserBundle as the parent of your bundle

```php
namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function getParent()
    {
        return 'BDSAUserBundle';
    }
}
```

## Create an empty User entity

```php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

use BDSA\UserBundle\Entity\User as BDSAUser;

/**
 * User
 * @ORM\Entity
 * @ORM\Table(name="bdsa_users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User  implements UserInterface, \Serializable
{
    use BDSAUser;
}
```

## Create UserRepository

```php
namespace AppBundle\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends \Doctrine\ORM\EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($email)
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
```

## Add this bundle to your security.yml

```yml
encoders:
    BDSA\UserBundle\Entity\User: bcrypt
    AppBundle\Entity\User: bcrypt

providers:
   bdsa_user_provider:
        entity:
            class: AppBundle:User

firewalls:

    # ...

    main:
        anonymous: ~
        logout:
            path: bdsa_user_logout
            target: /
        form_login:
            provider: bdsa_user_provider
            login_path: bdsa_user_login
            check_path: bdsa_user_login_check

access_control:
        - { path: /login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/, roles: IS_AUTHENTICATED_FULLY }
```

## Add routing to routing.yml

```yml
bdsa_user:
    resource: "@BDSAUserBundle/Resources/config/routing.yml"
```

## Default Configuration for your config.yml

Basically use `php bin/console config:dump-reference bdsa_user` to dump the default configuration.

```yml
# Default configuration for extension with alias: "bdsa_user"
bdsa_user:

    # This value is used in the request for the login field.
    authentication:       email # One of "email"; "username"; "both"

    # This value can only be used if "authentication" is set to "usermane" or "both".
    enable_username:      false
    email:                ~ # Required
    class: UserBundle\Entity\User
```

`php bin/console doctrine:schema:validate`

`php bin/console doctrine:schema:update --force`

[BDSA](http://bdsa.fr)