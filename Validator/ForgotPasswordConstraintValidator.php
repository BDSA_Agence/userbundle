<?php

namespace BDSA\UserBundle\Validator;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ForgotPasswordConstraintValidator extends ConstraintValidator
{
    private $em;
	private $class;

	public function __construct(EntityManager $em, $class)
	{
       $this->em    = $em;
       $this->class = $class;
	}

    public function validate($email, Constraint $constraint)
    {
    	$user = $this->em->getRepository($this->class)->findBy( array('email' => $email) );

    	if( empty($user) )
    	{
    		$this->context->buildViolation($constraint->message)
    		->setParameter('{{email}}', $email)
    		->addViolation();
    	}
    }
}