<?php

namespace BDSA\UserBundle\Validator;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ForgotPasswordConstraint extends Constraint
{
	public $message = 'bdsa.forgotpassword.request.violation';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}