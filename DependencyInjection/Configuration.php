<?php

namespace BDSA\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('bdsa_user');

        $rootNode
            ->children()
                ->enumNode('authentication')
                    ->info('This value is used in the request for the login field.')
                    ->values(array('email', 'username', 'both'))
                    ->defaultValue('email')
                    ->isRequired()
                ->end()
                ->booleanNode('enable_username')
                    ->info('This value can only be used if "authentication" is set to "usermane" or "both".')
                    ->defaultFalse()
                ->end()
                ->scalarNode('email')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('class')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
