<?php

namespace BDSA\UserBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Translation\TranslatorInterface;

use BDSA\UserBundle\Event\UserEvent;

class UserSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $twig;
    private $email;
    private $translator;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, TranslatorInterface $translator, $email)
    {
        $this->mailer     = $mailer;
        $this->twig       = $twig;
        $this->email      = $email;
        $this->translator = $translator;
    }

    public static function getSubscribedEvents()
    {
        return array(
            UserEvent::USER_REGISTRATION_SUCCESS   => 'onRegistrationSuccess',
            UserEvent::USER_REGISTRATION_PREFLUSH  => 'onRegistrationPreflush',
            UserEvent::USER_FORGOTPASSWORD_REQUEST => 'onForgotPasswordRequest',
            UserEvent::USER_FORGOTPASSWORD_RESET   => 'onForgotPasswordReset',
            UserEvent::USER_PROFILE_EDIT_POSTFLUSH => 'onProfileEditPostFlush'
        );
    }

    public function onRegistrationSuccess(UserEvent $event)
    {
        $user = $event->getUser();
    }

    public function onRegistrationPreflush(UserEvent $event)
    {
    	$user = $event->getUser();
    }

    public function onForgotPasswordRequest(UserEvent $event)
    {
        $user = $event->getUser();

        $user->setForgotPassword( sha1(uniqid()) );

        $subject = $this->translator->trans('bdsa.forgotpassword.email.request');

        $message = new \Swift_Message($subject);
        
        $message->setFrom($this->email);

        $message->setTo($user->getEmail());
        $message->setBody(
            $this->twig->render(
                'BDSAUserBundle:ForgotPassword:mail_request.html.twig',
                array(
                    'user' => $user
                )
            ),
            'text/html'
        );

        $this->mailer->send($message);
    }

    public function onForgotPasswordReset(UserEvent $event)
    {
        $user = $event->getUser();

        $user->setForgotPassword( null );
    }

    public function onProfileEditPostFlush(UserEvent $event)
    {
        $user = $event->getUser();
    }
}